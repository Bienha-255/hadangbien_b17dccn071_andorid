package com.example.hadangbien_btl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.hadangbien_btl.activity.BillActivity;
import com.example.hadangbien_btl.activity.MenuActivity;
import com.example.hadangbien_btl.databinding.ActivityMainBinding;
import com.example.hadangbien_btl.databinding.MenuBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.username.getText().toString().equals("hadangbien255") &&
                binding.password.getText().toString().equals("123456")){
                    startActivity(new Intent(MainActivity.this, MenuActivity.class));
                    finish();
                }
            }
        });
    }
}