package com.example.hadangbien_btl.retrofit;

import com.example.hadangbien_btl.model.Bill;
import com.example.hadangbien_btl.model.BillResponse;
import com.example.hadangbien_btl.model.Customer;
import com.example.hadangbien_btl.model.CustomerResponse;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface DataClient {
    @GET("bills")
    Call<BillResponse> getAllBill(@Query("month") int month, @Query("year") int year);

    @POST("send-mails")
    Call<Void> postBill(@Body List<Bill> bills);

    @POST("report-mails")
    Call<Void>postReport(@Body List<Bill> bills);


    @GET("customers")
    Call<CustomerResponse> getListCustomer(@Query("distric") String district,
                                           @Query("code") String code);

    @POST("customers")
    Call<CustomerResponse> create(@Body Map<String,Object> map);
}
