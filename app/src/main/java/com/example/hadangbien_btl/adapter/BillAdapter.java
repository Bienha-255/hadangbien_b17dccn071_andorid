package com.example.hadangbien_btl.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hadangbien_btl.R;
import com.example.hadangbien_btl.model.Bill;

import java.util.List;

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.ViewHolder> {

    private List<Bill> billList;
    private Context context;

    public BillAdapter(List<Bill> billList, Context context) {
        this.billList = billList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.line_bill, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BillAdapter.ViewHolder holder, int position) {
        Bill bill = billList.get(position);
        holder.txtLineName.setText(bill.toString());
    }



    @Override
    public int getItemCount() {
        return billList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView txtLineName;
        public ViewHolder(View itemView) {
            super(itemView);
            txtLineName = itemView.findViewById(R.id.line_name);
        }
    }
}
