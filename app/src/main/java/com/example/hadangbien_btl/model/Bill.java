package com.example.hadangbien_btl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bill {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("customer_code")
    @Expose
    private String customerCode;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("new_number")
    @Expose
    private Double newNumber;
    @SerializedName("old_number")
    @Expose
    private Double oldNumber;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("finish_date")
    @Expose
    private String finishDate;
    @SerializedName("total_price")
    @Expose
    private Integer totalPrice;
    @SerializedName("month")
    @Expose
    private Integer month;
    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("payment_date")
    @Expose
    private String paymentDate;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getNewNumber() {
        return newNumber;
    }

    public void setNewNumber(Double newNumber) {
        this.newNumber = newNumber;
    }

    public Double getOldNumber() {
        return oldNumber;
    }

    public void setOldNumber(Double oldNumber) {
        this.oldNumber = oldNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "Mã khách hàng :" + customerCode+", Số điện tiêu thụ: " + amount+ ", Tổng tiền: " +totalPrice+ ", Hạn nộp: " + paymentDate;
    }
}
