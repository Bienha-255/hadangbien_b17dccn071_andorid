package com.example.hadangbien_btl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Customer implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("identity_number")
    @Expose
    private Object identityNumber;
    @SerializedName("distric")
    @Expose
    private String distric;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("total_amount")
    @Expose
    private Float totalAmount;
    @SerializedName("active_at")
    @Expose
    private String activeAt;
    @SerializedName("bills")
    @Expose
    private Object bills;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(Object identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getDistric() {
        return distric;
    }

    public void setDistric(String distric) {
        this.distric = distric;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getActiveAt() {
        return activeAt;
    }

    public void setActiveAt(String activeAt) {
        this.activeAt = activeAt;
    }

    public Object getBills() {
        return bills;
    }

    public void setBills(Object bills) {
        this.bills = bills;
    }

    @Override
    public String toString() {
        return
                "Mã khách hàng :" + code+", Tên khách hàng: " + name+ ", Quận : " +distric+ ", Email: " + email;
    }

}
