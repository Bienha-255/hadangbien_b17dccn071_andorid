package com.example.hadangbien_btl.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BillResponse implements Serializable {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("items")
    @Expose
    private List<Bill> items = null;

    @SerializedName("total_items")
    @Expose
    private Integer totalItems;

    public BillResponse(String code, String message, Integer page, Integer size, List<Bill> items, Integer totalItems) {
        this.code = code;
        this.message = message;
        this.page = page;
        this.size = size;
        this.items = items;
        this.totalItems = totalItems;
    }

    public BillResponse() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<Bill> getItems() {
        return items;
    }

    public void setItems(List<Bill> items) {
        this.items = items;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }
}
