package com.example.hadangbien_btl.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.hadangbien_btl.Constant.Constants;
import com.example.hadangbien_btl.R;
import com.example.hadangbien_btl.adapter.BillAdapter;
import com.example.hadangbien_btl.adapter.CustomerAdapter;
import com.example.hadangbien_btl.databinding.ActivityCustomerBinding;
import com.example.hadangbien_btl.model.Customer;
import com.example.hadangbien_btl.model.CustomerResponse;
import com.example.hadangbien_btl.retrofit.APIUtils;
import com.example.hadangbien_btl.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerActivity extends AppCompatActivity {
    private ActivityCustomerBinding customerBinding;
    private List<Customer> customers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customerBinding = ActivityCustomerBinding.inflate(getLayoutInflater());
        setContentView(customerBinding.getRoot());
        customers = new ArrayList<>();
        customerBinding.btnAddCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerActivity.this, AddCustomerActivity.class));
            }

        });

        String[] quan = {"Hà Đông", "Nam Từ Liêm","Thanh Xuân","Đống Đa","Cầu Giấy", "Hai Bà Trưng"};
        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1,quan);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        customerBinding.spinner.setAdapter(arrayAdapter);

        customerBinding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quan = customerBinding.spinner.getSelectedItem().toString();
                Toast.makeText(CustomerActivity.this, quan, Toast.LENGTH_SHORT).show();
                String code = customerBinding.textCode.getText().toString();

                DataClient dataClient = APIUtils.getData(Constants.customer_url);
                Call<CustomerResponse> getList = dataClient.getListCustomer(quan, code);
                getList.enqueue(new Callback<CustomerResponse>() {
                    @Override
                    public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                        customers = response.body().getCustomers();
                        // xử lý adapter, recuclerview ở đây
                        Toast.makeText(CustomerActivity.this, customers.size() + "", Toast.LENGTH_SHORT).show();
                        Log.d("erro", customers.size() + "");
                        customerBinding.recycleView.setLayoutManager(new LinearLayoutManager(CustomerActivity.this));
                        CustomerAdapter adapter = new CustomerAdapter(customers, CustomerActivity.this);
                        customerBinding.recycleView.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<CustomerResponse> call, Throwable t) {
                        Log.d("erro", t.getMessage()+ "");
                    }
                });
            }
        });
//
//        customerBinding.btnAddCustomer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(CustomerActivity.this, AddCustomerActivity.class));
//            }
//
//        });
    }
}
