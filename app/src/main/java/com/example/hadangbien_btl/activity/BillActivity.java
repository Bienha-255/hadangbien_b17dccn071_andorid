package com.example.hadangbien_btl.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.hadangbien_btl.Constant.Constants;
import com.example.hadangbien_btl.R;
import com.example.hadangbien_btl.adapter.BillAdapter;
import com.example.hadangbien_btl.databinding.ActivityListbillBinding;
import com.example.hadangbien_btl.databinding.ActivitySendmailBinding;
import com.example.hadangbien_btl.model.Bill;
import com.example.hadangbien_btl.model.BillResponse;
import com.example.hadangbien_btl.retrofit.APIUtils;
import com.example.hadangbien_btl.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BillActivity extends AppCompatActivity {

    private ActivitySendmailBinding binding;
    private List<Bill> bills;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySendmailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        bills = new ArrayList<>();

        binding.button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int month = Integer.parseInt(binding.editTextNumber2.getText().toString());
                int year = Integer.parseInt(binding.editTextNumber.getText().toString());
                //Toast.makeText(BillActivity.this,binding.editTextNumber.getText() + "", Toast.LENGTH_LONG).show();
                DataClient dataClient = APIUtils.getData(Constants.base_url);
                Call<BillResponse> getListBill = dataClient.getAllBill(month, year);
                getListBill.enqueue(new Callback<BillResponse>() {
                    @Override
                    public void onResponse(Call<BillResponse> call, Response<BillResponse> response) {
                        bills = response.body().getItems();
                        Toast.makeText(BillActivity.this, bills.size() + "", Toast.LENGTH_SHORT).show();
                        Log.d("erro", bills.size() + "");
                        binding.recyclerView.setLayoutManager(new LinearLayoutManager(BillActivity.this));
                        BillAdapter adapter = new BillAdapter(bills, BillActivity.this);
                        binding.recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<BillResponse> call, Throwable t) {
                        Log.d("erro", t.getMessage()+ "");
                    }
                });
            }
        });

        binding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bills.size() > 0){
                    DataClient dataClient = APIUtils.getData(Constants.mail_url);
                    Call<Void> postBill = dataClient.postBill(bills);
                    postBill.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Toast.makeText(BillActivity.this, "Đã gửi " + bills.size() + " bills", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            Toast.makeText(BillActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        binding.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bills.size() > 0){
                    DataClient dataClient = APIUtils.getData(Constants.mail_url);
                    Call<Void> postBill = dataClient.postReport(bills);
                    postBill.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Toast.makeText(BillActivity.this, "Đã gửi " + bills.size() + " bills", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            Toast.makeText(BillActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

    }
}