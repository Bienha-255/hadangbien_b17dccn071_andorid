package com.example.hadangbien_btl.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hadangbien_btl.MainActivity;
import com.example.hadangbien_btl.databinding.ActivitySendmailBinding;
import com.example.hadangbien_btl.databinding.MenuBinding;

public class MenuActivity extends AppCompatActivity {
    private MenuBinding menuBinding;
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    menuBinding = MenuBinding.inflate(getLayoutInflater());
    setContentView(menuBinding.getRoot());
    menuBinding.btnMail.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                startActivity(new Intent(MenuActivity.this, BillActivity.class));
        }
    });
    menuBinding.btnCustomer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(MenuActivity.this, CustomerActivity.class));
        }

    });

    }

}
