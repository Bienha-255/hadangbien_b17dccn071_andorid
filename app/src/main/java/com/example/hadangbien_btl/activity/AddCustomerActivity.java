package com.example.hadangbien_btl.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.hadangbien_btl.Constant.Constants;
import com.example.hadangbien_btl.R;
import com.example.hadangbien_btl.databinding.ActivityAddcustomerBinding;
import com.example.hadangbien_btl.model.BillResponse;
import com.example.hadangbien_btl.model.Customer;
import com.example.hadangbien_btl.model.CustomerResponse;
import com.example.hadangbien_btl.retrofit.APIUtils;
import com.example.hadangbien_btl.retrofit.DataClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCustomerActivity extends AppCompatActivity {
    private ActivityAddcustomerBinding addcustomerBinding;
    private Customer customer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addcustomerBinding = ActivityAddcustomerBinding.inflate(getLayoutInflater());
        setContentView(addcustomerBinding.getRoot());

        String[] quan = {"Hà Đông", "Nam Từ Liêm","Thanh Xuân","Đống Đa","Cầu Giấy", "Hai Bà Trưng"};
        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1,quan);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        addcustomerBinding.spinner2.setAdapter(arrayAdapter);

        addcustomerBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String,Object> map = new HashMap<>();
                Float amount = Float.valueOf(addcustomerBinding.textAmout.getText().toString());
                String code = addcustomerBinding.TextCode.getText().toString();
                String name = addcustomerBinding.TextName.getText().toString();
                String identify = addcustomerBinding.TextIdentify.getText().toString();
                String address = addcustomerBinding.TextAddres.getText().toString();
                String distric = addcustomerBinding.spinner2.getSelectedItem().toString();
                String phone = addcustomerBinding.textPhone.getText().toString();
                String email = addcustomerBinding.TextEmail.getText().toString();
                map.put("code",code);
                map.put("total_amount",amount);
                map.put("address",address);
                map.put("name",name);
                map.put("identity_number",identify);
                map.put("distric",distric);
                map.put("phone",phone);
                map.put("email",email);
                DataClient dataClient = APIUtils.getData(Constants.base_url);
                Call<CustomerResponse> createCustomer = dataClient.create(map);
                createCustomer.enqueue(new Callback<CustomerResponse>() {

                    @Override
                    public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                        Toast.makeText(AddCustomerActivity.this, "Đã tạo " , Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AddCustomerActivity.this, CustomerActivity.class));
                    }

                    @Override
                    public void onFailure(Call<CustomerResponse> call, Throwable t) {
                        Toast.makeText(AddCustomerActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
